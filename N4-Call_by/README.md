![TBZ Logo](../x_gitressourcen/tbz_logo.png)
![m319 Picto](../x_gitressourcen/m319_picto.jpg)

[TOC]

# Abstraktionen mit Parameter


---

### Lernziel:
* Unterschied der zwei Paramaterübergabe-Konzepte. 
* Anwendung der Konzepte bei Variablen mit primitiven und komplexen Datentypen

---

# Paramaterübergabe:

Um Daten in einem Unterprogramm zu verwenden, werden die diese vorzugsweise in der Parameterliste eingetragen und so übergeben:

![Video:](../x_gitressourcen/Video.png) 5 min 
[![Tutorial](https://img.youtube.com/vi/2qFNi81dWuI/0.jpg)](https://www.youtube.com/watch?v=2qFNi81dWuI)

> **Merke**:
> 
> * Call-by-Value (Wertübergebe): Kopiert Wert und dann wird in der Methode/Funktion nur lokale Kopie geändert!
> * Call-by-Referenz (Variablenübergebe): Referenz auf Original wird übergeben und dann wird in der Methode/Funktion das Original geändert, und somit auch im Hauptprogramm! 

## Call by Value

Alle Variablen der **primitiven Typen** werden in JAVA immer **als Wert** übergeben!

Siehe Video oben!

[![Buch](../x_gitressourcen/Buch.jpg)Unterlagen Kap.5.2.2 -5.2.4](../Knowledge_Library/Java_Programmieren.pdf)


## Call-by-Referenz (für die Inhalte der komplexen Variablen)

Alle Variablen der **konmplexen Typen** werden in JAVA immer **als Referenz** übergeben! D.h. eine Veränderung des Inhalts der komplexen Variable wird auch im Hauptprogramm wirksam.

![Video:](../x_gitressourcen/Video.png) 5 min 
[![Tutorial](https://img.youtube.com/vi/6pqWEj2XVcQ/0.jpg)](https://www.youtube.com/watch?v=6pqWEj2XVcQ)


> **Achtung**: Eine Änderung der *Referenz* innerhalb der Funktion führt ** ** zur Veränderung der *Referenz* im Hauptprogramm!

---

![ToDo](../x_gitressourcen/ToDo.png) Do To:

[W3S Parameterübergabe](https://www.w3schools.com/java/java_methods_param.asp)


---

# Checkpoint
* Primitve Datentypen &#8594; Call-by-Value
* Komplexe Datentypen (Objekte) &#8594; Call-by-Reference *für den Inhalt der Objektvariable*
* Parameterliste und Rückgabewert
* Verhalten der Variableninhalte ausserhalb und innderhalb der Funktionen

