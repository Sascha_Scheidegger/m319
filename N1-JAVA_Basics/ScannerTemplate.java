import java.util.Scanner;

public class SimpleTemplate {
	
	// declare attributes here
	// we need the scanner to deal with user input
    static Scanner scanner;
 
    /**
     * constructor: initialize attributes
     */
    public SimpleTemplate() {
        scanner = new Scanner(System.in);
    }
 
    /**
     * our main function which runs the program
     * 
     * @param args is for command line arguments
     */
    public static void main(String[] args) {
        
    	//here we can define our logic:
        
        
        
    	
    	// command to close the scanner for performance reasons
        scanner.close();
    }
 
    

}
